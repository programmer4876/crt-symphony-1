FROM php:8-fpm-alpine
LABEL maintainer="programmer4876@gmail.com"
COPY . /usr/src/app
WORKDIR /usr/src/app